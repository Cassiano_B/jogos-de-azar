/**
 * O usuário faz uma escolha
 * O computador faz uma escolha aleatória
 * A função de comparação determina quem ganha
 */

const handleClick = (event) => {
    const button = event.target;
    
    let escolhaPlayer = button.id;
    let escolhaMaquina = randomPPT();

    switch(escolhaPlayer){
        case 'pedra':
            switch(escolhaMaquina){
                case 'pedra':
                    // empate
                    empate(escolhaPlayer, escolhaMaquina);
                    break;
                case 'papel':
                    // maquina ganha
                    maquinaVence(escolhaPlayer, escolhaMaquina);
                    break;
                case 'tesoura':
                    // player ganha
                    playerVence(escolhaPlayer, escolhaMaquina);
                    break;
            }
            break;
        case 'papel':
            switch(escolhaMaquina){
                case 'pedra':
                    // player ganha
                    playerVence(escolhaPlayer, escolhaMaquina);
                    break;
                case 'papel':
                    // empate
                    empate(escolhaPlayer, escolhaMaquina);
                    break;
                case 'tesoura':
                    // maquina ganha
                    maquinaVence(escolhaPlayer, escolhaMaquina);
                    break;
            }
            break;
        case 'tesoura':
            switch(escolhaMaquina){
                case 'pedra':
                    // maquina ganha
                    maquinaVence(escolhaPlayer, escolhaMaquina);
                    break;
                case 'papel':
                    // player ganha
                    playerVence(escolhaPlayer, escolhaMaquina);
                    break;
                case 'tesoura':
                    // empate
                    empate(escolhaPlayer, escolhaMaquina);
                    break;
            }
            break;
        default:
            alert("Deu ruim");
            break;
    }
}

function randomPPT(){
    let random = Math.ceil(Math.random() * 3);
    switch(random){
        case 1:
            return 'pedra';
        case 2:
            return 'papel';
        default:
            return 'tesoura'
    }
}

function createElements(msg){
    const resultado = document.querySelector("#resultado");
    let text = document.createElement("p");
    text.textContent = msg;
    resultado.appendChild(text);

    modificaMarcador();
}

function playerVence(player, maquina){
    marcador.player++;
    let msg = `Player usou ${player}, maquina usou ${maquina}. O vencedor é PLAYER.`;
    createElements(msg);
}

function maquinaVence(player, maquina){
    marcador.maquina++;
    let msg = `Player usou ${player}, maquina usou ${maquina}. O vencedor é MAQUINA`;
    createElements(msg);
}

function empate(player, maquina){
    marcador.empate++;
    let msg = `Player usou ${player}, maquina usou ${maquina}. É um empate.`;
    createElements(msg);
}

function modificaMarcador(){
    const marcadorHtml = document.querySelector("#marcador");
    marcadorHtml.innerHTML = "Vitórias do PLAYER: " + marcador.player + ".<br>Vitórias da MAQUINA " + 
    marcador.maquina + ".<br>Empates: " + marcador.empate + ".";
}

let buttons = document.querySelectorAll(".button");

for(let i = 0; i < buttons.length; i++){
    buttons[i].addEventListener("click", handleClick);
}

const marcador = {
    player: 0,
    maquina: 0,
    empate: 0
};

modificaMarcador();