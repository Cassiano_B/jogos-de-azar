const respostas = [
    //sim
    "É certo.", "É decididamente verdade.", "Sem dúvida.", "Sim - definitivamente.", "Você pode confiar nisso.",
    "Pelo que vejo, sim.", "Provavelmente.", "Prognóstico bom.", "Sim.", "Os sinais apontam para sim.", 
    //talvez
    "Resposta nebulosa, tente novamente.", "Pergunte novamente mais tarde.", "Melhor não contar agora.",
    "Não é possível prever agora.", "Concentre-se e pergunte novamente.", 
    //não
    "Não conte com isso.",
    "Minha resposta é não.", "Minhas fontes dizem não.", "O prognóstico não é tão bom.", "Muito duvidoso."
];

function answer(){
    if(document.querySelector("#pergunta").value.includes("?")){
        let num = Math.floor(Math.random() * respostas.length);

        let resposta = respostas[num];
        let oito = document.querySelector("#eight");
        if(oito == null || oito == undefined){
            oito = document.querySelector("#resposta");
        }
        oito.id = "resposta";
        oito.textContent = resposta;
    }else{
        if(document.querySelector("#pergunta").value.length >= 1){
            alert("Faça uma pergunta de resposta SIM ou NÃO. (?)");
        }else{
            alert("Faça uma pergunta.");
        }
    }
}