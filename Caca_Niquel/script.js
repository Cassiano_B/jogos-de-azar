let rolar = false;

let numLeft = 0;
let numCenter = 0;
let numRight = 0;

let cont = 0;

function comecaARolar(){
    rolar = true;
    cont = 0;
    rolarNumeros();
}

function escolheRolar(){
    if(rolar){
        rolar = false;
    }
    else{
        comecaARolar();
    }
}

function rolarNumeros(){
    let random = setInterval(function(){
        if(rolar){
            numLeft = Math.floor(Math.random() * 10);
            numCenter = Math.floor(Math.random() * 10);
            numRight = Math.floor(Math.random() * 10);
        
            let slots = document.querySelectorAll(".slot");
        
            slots[0].textContent = numLeft;
            slots[1].textContent = numCenter;
            slots[2].textContent = numRight;
        }else{
            if(cont == 0){
                let resultado = document.querySelector("#resultado");
                if((numLeft == numCenter) || (numLeft == numRight) || (numCenter == numRight)){
                    marcador.duplos++;
                    resultado.textContent = "Parabéns você acertou dois.";
                }else if((numLeft == numCenter) && (numLeft == numRight)){
                    marcador.completos++;
                    resultado.textContent = "Parabéns você acertou todos!!!";
                }else{
                    marcador.erros++;
                    resultado.textContent = "Que pena... Você errou.";
                }

                marcador.tentativas++;
                modificaMarcador();
                cont++;
            }
        }
    }, 100);
}

let button = document.querySelector("#rolar");
button.addEventListener("click", escolheRolar);

const marcador = {
    tentativas: 0,
    completos: 0,
    duplos: 0,
    erros: 0
}

function modificaMarcador(){
    let tentativas = document.getElementById("tentativas");
    let completos = document.getElementById("completos");
    let duplos = document.getElementById("duplos");
    let erros = document.getElementById("erros");

    tentativas.textContent = marcador.tentativas;
    completos.textContent = marcador.completos;
    duplos.textContent = marcador.duplos;
    erros.textContent = marcador.erros;
}

modificaMarcador();