function createTable(tamanhoTabela = 10){
    let text = document.getElementById("text");
    text.textContent = "";

    let table = document.querySelector("#palavras");
    table.innerHTML = '';

    let gridTable = {};
    for(let row = 0; row < tamanhoTabela; row++){
        let tr = document.createElement("tr");
        tr.id = "row_" + row;
        gridTable[row] = [];
        for(let column = 0; column < tamanhoTabela; column++){
            let td = document.createElement("td");
            td.id = "row_" + row + "_column_" + column;
            tr.appendChild(td);
            gridTable[row][column] = [];
            gridTable[row][column] = td;
        }
        table.appendChild(tr);
    }

    let palavras = escolherPalavras();

    for(let i = 0; i < quantPalavras; i++){
        let randomRow = Math.floor(Math.random() * tamanhoTabela);
        let randomColumn = Math.floor(Math.random() * (tamanhoTabela - palavras[i].length));

        while(usedRowsColumns.filter((arr) => arr.includes(randomRow)).length > 0){
            randomRow = Math.floor(Math.random() * tamanhoTabela);
        }

        usedRowsColumns[i] = [randomRow, []];
        
        for(let letters = 0; letters < palavras[i].length; letters++){
            usedRowsColumns[i][1].push(randomColumn);
            gridTable[randomRow][randomColumn++].textContent = palavras[i][letters];
        }
    }

    for(let i = 0; i < tamanhoTabela; i++){
        for(let j = 0; j < tamanhoTabela; j++){
            if(gridTable[i][j].textContent == ''){
                let randomIndex = Math.floor(Math.random() * letras.length);

                gridTable[i][j].textContent = letras[randomIndex];
            }
            gridTable[i][j].addEventListener("click", clicaLetra);
        }
    }
}

function escolherPalavras(){
    let arrPalavras = [];
    for(let i = 0; i < quantPalavras; i++){
        let randomIndex = Math.floor(Math.random() * palavrasFeitas.length);

        while(arrPalavras.includes(palavrasFeitas[randomIndex])){
            randomIndex = Math.floor(Math.random() * palavrasFeitas.length);
        }
        
        arrPalavras[i] = palavrasFeitas[randomIndex];
    }
    return arrPalavras;
}

function clicaLetra(event){
    let target = event.target;

    let parent = target.parentNode;
    
    for(let i = 0; i < usedRowsColumns.length; i++){
        if(usedRowsColumns[i][0] == parent.id.slice(-1)){
            //tem na row
            if(usedRowsColumns[i][1].includes(Number(target.id.slice(-1)))){
                let msg = '';
                //tem na column
                for(let j of usedRowsColumns[i][1]){
                    parent.children[j].id = 'aaa';
                    parent.children[j].className = "selecionado";
                    msg += parent.children[j].textContent;
                }

                quant++;
            }
        }
    }

    if(quant == quantPalavras){
        win();
    }
}

function win(){
    quant = 0;
    usedRowsColumns = [];

    let text = document.getElementById("text");
    text.textContent = "Você ganhou!!! Aperte no botão para jogar novamente! ";


    let button = document.createElement("button");
    button.textContent = "Jogar novamente";
    button.addEventListener("click", () => createTable());
    text.appendChild(button);
}

const palavrasFeitas = [
    "Estupefar", "Exaurido", "Cacofonia", "Custoso", "Firmino", 
    "Amplexo", "Cachorro", "Oculos", "Irritar", "Forma",
    "Bebado", "Aspirina", "Molhar", "Dentes", "Pedreiro", 
    "Machado", "Rosa", "Topicos", "Galope", "Osso"
];
palavrasFeitas.forEach( palavra => {
    palavrasFeitas[palavrasFeitas.indexOf(palavra)] = palavra.toUpperCase();
});

const letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

let usedRowsColumns = [];

let quantPalavras = 3;
let quant = 0;

createTable();